#include "linear.h"
#include "echo.h"
#include "echo_util.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double time_diff(struct timeval t1, struct timeval t0){
  long long elapsed = (t1.tv_sec-t0.tv_sec)*1000000LL + t1.tv_usec-t0.tv_usec;
  return elapsed / (double)1000000;
}

int main(int argc, char** argv) {
    if(argc < 7) {
	printf("Usage: %s input target test_input test_target valid_input valid_target (lag) (replicates)\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));

    const unsigned int input_dim  = 1;
    const unsigned int output_dim = 1;

    const unsigned int lag = 
	argc < 8 ? 10 : atoi(argv[7]);

    const unsigned int replicates = 
	argc < 9 ? 100 : atoi(argv[8]);

    /* const unsigned int trimmed_es =  */
    /* 	argc < 10 ? 0 : atoi(argv[9]); */

    FILE* f_input = fopen(argv[1], "r");
    if(!f_input) {
	printf("Unable to read input file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_series* input =
	echo_util_read_doubles(f_input, &chol);
    fclose(f_input);

    FILE* f_target = fopen(argv[2], "r");
    if(!f_target) {
	printf("Unable to read target file: %s\n", argv[2]);
	exit(EXIT_FAILURE);
    }
    echo_series* target_data =
	echo_util_read_doubles(f_target, &chol);
    fclose(f_target);

    FILE* f_test_input = fopen(argv[3], "r");
    if(!f_test_input) {
	printf("Unable to read test_input file: %s\n", argv[3]);
	exit(EXIT_FAILURE);
    }
    echo_series* test_input =
	echo_util_read_doubles(f_test_input, &chol);
    fclose(f_test_input);

    FILE* f_test_target = fopen(argv[4], "r");
    if(!f_test_target) {
	printf("Unable to read test_target file: %s\n", argv[4]);
	exit(EXIT_FAILURE);
    }
    echo_series* test_target_data =
	echo_util_read_doubles(f_test_target, &chol);
    fclose(f_test_target);

    FILE* f_valid_input = fopen(argv[5], "r");
    if(!f_valid_input) {
	printf("Unable to read valid_input file: %s\n", argv[3]);
	exit(EXIT_FAILURE);
    }
    echo_series* valid_input =
	echo_util_read_doubles(f_valid_input, &chol);
    fclose(f_valid_input);

    FILE* f_valid_target = fopen(argv[6], "r");
    if(!f_valid_target) {
	printf("Unable to read valid_target file: %s\n", argv[4]);
	exit(EXIT_FAILURE);
    }
    echo_series* valid_target_data =
	echo_util_read_doubles(f_valid_target, &chol);
    fclose(f_valid_target);

    double* train_mse = malloc(sizeof(double)*replicates*60);
    double* test_mse  = malloc(sizeof(double)*replicates*60);
    double* valid_mse = malloc(sizeof(double)*replicates*60);

    echo_network net = 
	echo_util_make_network( 1, input_dim, output_dim
			      , gen, &chol );

    /* if (trimmed_es){ */
    /* 	linear_sparse_matrix_free(net.dynamics, &chol); */
    /* 	net.dynamics = echo_util_generate_trimmed_dynamics(initial_reservoir_size, gen, &chol); */
    /* } */

    echo_series *target = 
	echo_util_output_series_for(net, target_data->nrow - lag, &chol);
    memcpy( target->x
	  , ((double*)target_data->x) + lag
	  , (target->nrow)*sizeof(double) );

    echo_series *test_target = 
	echo_util_output_series_for(net, test_target_data->nrow - lag, &chol);
    memcpy( test_target->x
	  , ((double*)test_target_data->x) + lag
	  , (test_target->nrow)*sizeof(double) );

    echo_series *valid_target = 
	echo_util_output_series_for(net, valid_target_data->nrow - lag, &chol);
    memcpy( valid_target->x
	  , ((double*)valid_target_data->x) + lag
	  , (valid_target->nrow)*sizeof(double) );

    double mse;
    double residual;

    unsigned int* rs = malloc(sizeof(unsigned int)*replicates*60);

    double* ptr_run;
    double* ptr_target;
    echo_series* run;
    const unsigned int run_length = test_target->nrow;

    double* spec_radii = malloc(sizeof(double)*replicates*60);
    double* dist_maxes = malloc(sizeof(double)*replicates*60);
    double* dist_conns = malloc(sizeof(double)*replicates*60);

    time_t* at_times = malloc(sizeof(time_t)*replicates*60);
    time_t base_time;

    for(int n = 0; n < 60; ++n) {
	base_time = clock();
    for(int i = 0; i < replicates; ++i) {
	const unsigned int reservoir_size = 5*(n+1);
	rs[n*replicates + i] = reservoir_size;
	
	linear_sparse_matrix_free(net.dynamics, &chol);
	linear_dense_matrix_free(net.readin, &chol);
	linear_dense_matrix_free(net.readout, &chol);

	net.dynamics = (echo_dynamics*)0;
	double spec_rad;
	double params[2];
	while(!net.dynamics) {
	    spec_rad = 2.9*gsl_rng_uniform(gen) + 0.1;
	    params[0] = 0.3*gsl_rng_uniform(gen) + 0.05;
	    params[1] = gsl_rng_uniform(gen) + 0.3;

	    for(int retry = 0; retry < 5; ++retry) {
		net.dynamics = echo_util_generate_es_dynamics_with( reservoir_size
						                  , spec_rad
							          , sparse_dist_nondumb
								  , (const void*) params
								  , gen
								  , &chol );
		if(net.dynamics) break;
	    }
	}

	dist_maxes[n*replicates + i] = params[1];
	dist_conns[n*replicates + i] = params[0];
	spec_radii[n*replicates + i] = spec_rad;

	net.readin = linear_dense_matrix_random( reservoir_size
					       , 1 , gen
					       , &dense_dist, (void*)0
					       , &chol );
	net.readout = linear_dense_matrix_alloc(1, reservoir_size + 2, &chol);

	echo_util_train_network(net, lag, input, target, gen, &chol);

	//TODO: Write an unsafe version, so that we can reuse run!
	run = echo_util_eval_network_all(net, lag, input, gen, &chol);

	// TODO: Extract this and other useful statistical procedures
	// into a separte thing!
	mse = 0;
	ptr_run    = (double*)run->x;
	ptr_target = (double*)target->x;
	for(int j = 0; j < target -> nrow; ++j) {
	    residual = ptr_run[j] - ptr_target[j];
	    mse += residual*residual;
	} 
	train_mse[n*replicates + i] = mse/target -> nrow;
	at_times[n*replicates + i] = clock() - base_time;

	echo_util_series_free(run, &chol);
	run = echo_util_eval_network_all(net, lag, test_input, gen, &chol);

	mse = 0;
	ptr_run    = (double*)run->x;
	ptr_target = (double*)test_target->x;
	for(int j = 0; j < run_length; ++j) {
	    residual = ptr_run[j] - ptr_target[j];
	    mse += residual*residual;
	} 
	test_mse[n*replicates + i] = mse/run_length;
	fprintf(stderr, "%f\n", mse/run_length);

	echo_util_series_free(run, &chol);
	run = echo_util_eval_network_all(net, lag, valid_input, gen, &chol);

	mse = 0;
	ptr_run    = (double*)run->x;
	ptr_target = (double*)valid_target->x;
	for(int j = 0; j < run_length; ++j) {
	    residual = ptr_run[j] - ptr_target[j];
	    mse += residual*residual;
	} 
	valid_mse[n*replicates + i] = mse/run_length;

	echo_util_series_free(run, &chol);
    }}

    printf("Test.MSE\tTrain.MSE\tValid.MSE\tSize\tTime\tSpectral\tConnectivity\tSupport\n");
    for(int i = 0; i < 60*replicates; ++i)
	printf( "%f\t%f\t%f\t%d\t%Lf\t%f\t%f\t%f\n"
	      , test_mse[i]
	      , train_mse[i]
	      , valid_mse[i]
	      , rs[i]
	      , ((long double)at_times[i])/((long double)CLOCKS_PER_SEC)
	      , spec_radii[i]
	      , dist_conns[i]
	      , dist_maxes[i] );

    free(at_times);
    free(spec_radii);
    free(dist_maxes);
    free(dist_conns);
    free(train_mse);
    free(test_mse);
    free(valid_mse);

    echo_util_net_free(net, &chol);
    echo_util_series_free(input, &chol);
    echo_util_series_free(target, &chol);
    echo_util_series_free(target_data, &chol);
    echo_util_series_free(test_input, &chol);
    echo_util_series_free(test_target, &chol);
    echo_util_series_free(test_target_data, &chol);

    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
