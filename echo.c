/*********************************************************************************/
/* The MIT License (MIT)                                                         */
/*                                                                               */
/* Copyright (c) 2013 Tilo Wiklund, Ross Linscott                                */
/*                                                                               */
/* Permission is hereby granted, free of charge, to any person obtaining a copy  */
/* of this software and associated documentation files (the "Software"), to deal */
/* in the Software without restriction, including without limitation the rights  */
/* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     */
/* copies of the Software, and to permit persons to whom the Software is         */
/* furnished to do so, subject to the following conditions:                      */
/*                                                                               */
/* The above copyright notice and this permission notice shall be included in    */
/* all copies or substantial portions of the Software.                           */
/*                                                                               */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   */
/* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     */
/* THE SOFTWARE.                                                                 */
/*********************************************************************************/

#include <string.h>
#include <math.h>

#include "echo.h"

double identity(double x) { 
    return x; 
}

// TODO: Currently no bias, we probably want to implement it using a separate
// array in the the dynamics rather than fiddling with the arrays.

void check_valid_dynamics( const char* name
			 , const echo_dynamics const * dynamics) {
    if(dynamics->nrow != dynamics->ncol) {
        printf( "%s: Assymetric dynamics (Got %zu x %zu matrix)!\n"
	      , name, dynamics->nrow, dynamics->ncol );
	exit(EXIT_FAILURE);
    }
}

void check_compatible_state( const char* name
			   , const echo_state const * state
			   , const echo_dynamics const * dynamics) {
    if(state->ncol != 1) {
	printf( "%s: Non-vector state (Got %zu x %zu matrix)!\n"
	      , name, state->nrow, state->ncol );
	exit(EXIT_FAILURE);
    } 
    if(state->nrow != dynamics->ncol) {
	printf( "%s: Incompatible state/dynamics (%zu rows vs. %zu columns)!\n"
	      , name, state->nrow, dynamics->ncol );
	exit(EXIT_FAILURE);
    }
}

void check_compatible_readin( const char* name
			    , const echo_readin const * readin
			    , const echo_series const * input
			    , const echo_dynamics const * dynamics ) {
    if(readin->ncol != input->ncol) {
	printf( "%s: Incompatible readin/input (%zu columns vs. %zu rows)!\n"
	      , name, readin->ncol, input->nrow );
	exit(EXIT_FAILURE);
    }
    if(readin->nrow != dynamics->nrow) {
	printf( "%s: Incompatible readin/dynamics (%zu rows vs. %zu x %zu dynamics)!\n"
	      , name, readin->nrow, dynamics->nrow, dynamics->ncol );
        exit(EXIT_FAILURE);
    }
}

void check_compatible_readout( const char* name
			     , const echo_readout const * readout
			     , const echo_dynamics const * dynamics
			     , const int input_size) {
    if(readout->ncol != dynamics->nrow + 1 + input_size) {
	printf( "%s: Incompatible readout/dynamics (%zu cols vs. %zu x %zu dynamics)!\n"
	      , name, readout->ncol, dynamics->nrow, dynamics->ncol );
        exit(EXIT_FAILURE);
    }
}

void check_enough_output( const char* name
			, const echo_series const * output
			, const unsigned int length) {
    if(output->nrow < length) {
	printf( "%s: Insufficient space in output (got %zu, required %d)\n"
	      , name, output->nrow, length );
        exit(EXIT_FAILURE);
    }
}

void check_valid_output_width( const char* name
			     , const echo_series const * output
			     , const unsigned int width ) {
    if(output->ncol != width) {
	printf( "%s: Incompatible output (got %zu cols, required %d)\n"
	      , name, output->ncol, width );
        exit(EXIT_FAILURE);
    }
}

void check_valid_train( const char* name
                      , const unsigned int lag
	              , echo_series* input
	              , echo_series* target ) {
    if(target->ncol != 1) {
	printf( "%s: Only univariate output allowed (got %zu rows)\n"
	      , name, target->ncol );
        exit(EXIT_FAILURE);
    }
    const unsigned int expected = input->ncol - lag;
    if(target->nrow > expected) {
	printf( "%s: Too many targets (got %zu, but can only handle %d))\n"
	      , name, target->nrow, expected );
        exit(EXIT_FAILURE);
    }
}

void dump_matlab(dense_matrix* m){
  int i,j;
  for (i = 0; i < m->ncol; i++){
    for (j = 0; j < m->nrow; j++){
      fprintf(stdout,"%e\t", ((double*)m->x)[i * m->nrow + j]);
    }
    fprintf(stdout,"\n");
  }
}

// TODO: Combine steps and push, internally, into one procedure (with the latter
// given by out_output = 0), but export the same interface as before (I don't 
// like passing null-pointers having any reasonable semantics).

// stores *current* in first out_output, then stores num-1 consecutive steps in out_output
void echo_steps_n( const unsigned int num 
                 , echo_dynamics* dynamics , echo_state* current
		 , echo_readin* readin     , echo_series* input
                 , dense_matrix* out_output
                 , cholmod_common* chol ) {
    #ifndef NDEBUG
    check_valid_dynamics(    "echo_steps_n", dynamics                         );
    check_compatible_state(  "echo_steps_n", current    , dynamics            );
    check_compatible_readin( "echo_steps_n", readin     , input    , dynamics );
    check_enough_output(     "echo_steps_n", out_output , dynamics->nrow + 1 + input->ncol); //output dimension
    check_valid_output_width("echo_steps_n", out_output , num                 ); //output length
    #endif

    const unsigned int reservoir_size  = dynamics->nrow;
    int i;

    // For iterating over the output (we want to be able to treat each column as
    // a vector for matrix-vector multiplication)
    dense_sub_matrix* out_states = 
	linear_dense_matrix_slice_rows( 0
				      , reservoir_size
				      , out_output
				      , chol );
    dense_sub_matrix* out_bias = 
	linear_dense_matrix_slice_rows( reservoir_size
				      , reservoir_size + 1
				      , out_output
				      , chol );
    dense_sub_matrix* out_input = 
	linear_dense_matrix_slice_rows( reservoir_size + 1
				      , reservoir_size + 1 + input->ncol
				      , out_output
				      , chol );

    dense_matrix_column* view_curr  = linear_dense_matrix_columns(out_states, chol);
    dense_matrix_column* view_prev  = linear_dense_matrix_columns(out_states, chol);
    dense_matrix_column* view_bias = linear_dense_matrix_columns(out_bias, chol);
    dense_matrix_column* view_outinput = linear_dense_matrix_columns(out_input, chol);
    linear_next_column(view_curr, chol);

    // For iterating over the sequence of input values (TODO: There is 
    // conceptually no reason to model this as a matrix, perhaps there is some 
    // better way?)
    dense_matrix_row* view_input = linear_dense_matrix_rows(input, chol);

    // First value of output will be the initial state
    memcpy(view_prev->x, current->x, sizeof(double)*reservoir_size);
    
    /* ((double*)view_prev->x)[reservoir_size] = 1; */
    /* for (i = 0; i < input->ncol; i++){ */
    /*   ((double*)view_prev->x)[reservoir_size + i + 1] = ((double*)input->x)[i * input->nrow]; */
    /* } */

    // Temporary buffer for storing activation from input nodes
    echo_state* from_input = linear_dense_vector_alloc(reservoir_size, chol);

    // Statically computed/allocated for use when applying the 
    // activation function
    double* state_iter_ptr;

    /* fprintf(stdout,"steps: given current: \n"); */
    /* for (i = 0; i < view_prev->nzmax; i++){ */
    /*   fprintf(stdout,"%.2f, ", ((double*)view_prev->x)[i]); */
    /* } */
    /* fprintf(stdout,"\n"); */

    // We count the initial state, already copied over, as "i = 0"
    for(i = 1; i < num; ++i) {
        // Reads the previous state (pointed to by view_prev) and writes into
	// the next one (pointed to by view_curr).
	linear_sparse_apply(dynamics, view_prev, view_curr, chol);
	linear_dense_apply(readin, view_input, from_input, chol);
	linear_unsafe_dense_vector_add(view_curr, from_input, chol);

	// Activation function is applied pointwise
	state_iter_ptr = (double*)view_curr->x;
	
	for(int l = 0; l < reservoir_size; ++l) {
	  state_iter_ptr[l] = ECHO_ACTIVATION(state_iter_ptr[l]);
	  /* fprintf(stdout,"%.2f, ", state_iter_ptr[l]); */
	}
	/* fprintf(stdout,"\n"); */
	/* dense_vector* test = cholmod_copy_dense(view_prev, chol); */
	/* linear_unsafe_dense_vector_scale(-1, test, chol); */
	/* linear_unsafe_dense_vector_add(test, view_curr, chol); */
	/* fprintf(stdout,"norm: %.3e, difference: %.3e.\nsteps: ", cholmod_norm_dense(view_curr, 2, chol), cholmod_norm_dense(test, 2, chol)); */
	/* linear_dense_vector_free(test, chol); */

	((double*)view_bias->x)[0] = 1;
	linear_dense_vector_copy_to(view_outinput, view_input, 0, chol);
	
	linear_next_column(view_bias, chol);
	linear_next_column(view_outinput, chol);
	linear_next_column(view_curr, chol);
	linear_next_column(view_prev, chol);
	linear_next_row(view_input, chol);
    }

    // Fill in the final two values
    ((double*)view_bias->x)[0] = 1;
    linear_dense_vector_copy_to(view_outinput, view_input, 0, chol);

    linear_dense_vector_free(from_input, chol);

    linear_dense_matrix_rows_free(view_input, chol);
    linear_dense_matrix_columns_free(view_curr, chol);
    linear_dense_matrix_columns_free(view_prev, chol);
    linear_dense_matrix_columns_free(view_bias, chol);
    linear_dense_matrix_columns_free(view_outinput, chol);

    linear_dense_sub_matrix_free(out_states, chol);
    linear_dense_sub_matrix_free(out_bias, chol);
    linear_dense_sub_matrix_free(out_input, chol);
}

void echo_push_n( const unsigned int num        
                , echo_dynamics* dynamics , echo_state* initial 
		, echo_readin* readin     , echo_series* input
                , echo_state* out_output
		, cholmod_common* chol ) {
    echo_state* initial_copy = cholmod_copy_dense(initial, chol);
    echo_unsafe_push_n(num, dynamics, initial_copy, readin, input, out_output, chol);
    linear_dense_vector_free(initial_copy, chol);
}

// NOTE: echo states should be column vectors. 
// the result of iterating the dynamics num times is stored in out_output,
// the resulting state of *initial* is undefined.
void echo_unsafe_push_n( const unsigned int num
		       , echo_dynamics* dynamics , echo_state* initial
		       , echo_readin* readin     , echo_series* input
		       , echo_state* out_output
		       , cholmod_common* chol ) {
    #ifndef NDEBUG
    check_valid_dynamics(    "echo_push_n", dynamics                         );
    check_compatible_state(  "echo_push_n", initial    , dynamics            );
    check_compatible_readin( "echo_push_n", readin     , input    , dynamics );
    check_valid_output_width("echo_push_n", out_output , 1                   );
    #endif

    dense_matrix_row* view_input = linear_dense_matrix_rows(input, chol);

    const unsigned int reservoir_size = dynamics->nrow;

    echo_state* from_input = linear_dense_vector_alloc(reservoir_size, chol);

    double* state_iter_ptr; 

    echo_state* swap;
    // NOTE: Since we do not wish to store the intermediate states of the 
    // reservoir we can use a from/back-buffer strategy.
    for(int i = 0; i < num; ++i) { 
	linear_sparse_apply(dynamics, initial, out_output, chol);
	linear_dense_apply(readin, view_input, from_input, chol);
	linear_unsafe_dense_vector_add(out_output, from_input, chol);

	state_iter_ptr = (double*)out_output->x;
	for(int l = 0; l < reservoir_size; ++l) {
	  state_iter_ptr[l] = ECHO_ACTIVATION(state_iter_ptr[l]);
	  /* fprintf(stdout,"%.2f, ", state_iter_ptr[l]); */
	}
	/* fprintf(stdout,"\npush: "); */

	swap = out_output;
        out_output = initial;
	initial = swap;

	linear_next_row(view_input, chol);
    }

    linear_dense_matrix_rows_free(view_input, chol);
    linear_dense_vector_free(from_input, chol);

    // Make sure the data ends up in the correct vector (since they flip-flop within the loop)
    if(num % 2 == 0) {
	//cholmod_copy_dense2(initial, out_output, chol);
	memcpy(out_output->x, initial->x, sizeof(double)*reservoir_size);
    }
}

/* PUBLIC INTERFACE */

void echo_run_n( const unsigned int num , const unsigned int lag
	       , echo_network net       , echo_state* initial
               , echo_series* input
	       , echo_series* out_output
               , cholmod_common* chol ) {
    echo_state* initial_copy = cholmod_copy_dense(initial, chol);
    echo_unsafe_run_n(num, lag, net, initial_copy, input, out_output, chol);
    linear_dense_vector_free(initial_copy, chol);
}


/* Push lag steps, then perform num-lag steps and store the readout result in out_output 
   The resulting state is stored in initial.
 */
void echo_unsafe_run_n( const unsigned int num , const unsigned int lag
		      , echo_network net       , echo_state* initial
                      , echo_series* input
	              , echo_series* out_output
                      , cholmod_common* chol ) {
    echo_dynamics* dynamics = net.dynamics;
    echo_readout*  readout  = net.readout;
    echo_readin*   readin   = net.readin;

    #ifndef NDEBUG
    // TODO: We need some additional validation here!
    check_valid_dynamics(    "echo_run_n", dynamics                         );
    check_compatible_state(  "echo_run_n", initial    , dynamics            );
    check_compatible_readin( "echo_run_n", readin     , input    , dynamics );
    check_enough_output(     "echo_run_n", out_output , num - lag           );
    check_valid_output_width("echo_run_n", out_output , 1                   );
    #endif

    const unsigned int reservoir_size  = dynamics->nrow;

    echo_state* after_lag = linear_dense_vector_alloc(reservoir_size, chol);

    // Throw out the reservoir states we aren't interested in
    echo_unsafe_push_n(lag, dynamics, initial, readin, input, after_lag, chol);
    
    dense_sub_matrix* input_after_lag = 
	linear_dense_matrix_slice_rows(lag, input->nrow, input, chol);

    dense_matrix_row* view_input  = linear_dense_matrix_rows(input_after_lag, chol);
    dense_matrix_row* view_output = linear_dense_matrix_rows(out_output, chol);

    // Temporary buffer for storing activation from input nodes
    echo_state* from_input = linear_dense_vector_alloc(reservoir_size, chol);

    echo_state* state_backbuf = linear_dense_vector_alloc(reservoir_size, chol);
    dense_vector* ext = linear_dense_vector_alloc(reservoir_size + 1 + input->ncol, chol);

    // Statically computed/allocated for use when applying the 
    // activation function
    double* state_iter_ptr;

    dense_sub_matrix* input_ext = linear_dense_matrix_slice_rows(reservoir_size + 1, reservoir_size + 1 + input->ncol, ext, chol);

    echo_state* swap;
    ((double*)ext->x)[reservoir_size] = 1;

          
    for(int i = 0; i < num - lag; ++i) {
	linear_dense_vector_copy_to(ext, after_lag, 1, chol);
	linear_dense_vector_copy_to(input_ext, view_input, 0, chol);
      
        linear_dense_apply(readout, ext, view_output, chol); //consider moving to after first iteration?
        linear_sparse_apply(dynamics, after_lag, state_backbuf, chol); //internal dynamics
        linear_dense_apply(readin, view_input, from_input, chol); //input dynamics
	linear_unsafe_dense_vector_add(state_backbuf, from_input, chol); //combine

	// Activation function is applied pointwise
	state_iter_ptr = (double*)state_backbuf->x;
	for(int l = 0; l < reservoir_size; ++l) {
	  state_iter_ptr[l] = ECHO_ACTIVATION(state_iter_ptr[l]);
	  /* fprintf(stdout,"%.2f, ", state_iter_ptr[l]); */
	}
	/* fprintf(stdout,"\nrun: "); */

	linear_next_row(view_output, chol);
	linear_next_row(view_input, chol);

	swap = after_lag;
	after_lag = state_backbuf;
	state_backbuf = swap;
    }

    // write the resulting state to initial
    linear_dense_vector_copy_to(initial, after_lag, 1, chol);

    linear_dense_vector_free(from_input, chol);
    linear_dense_vector_free(ext, chol);


    linear_dense_matrix_rows_free(view_output, chol);
    linear_dense_matrix_rows_free(view_input, chol);

    linear_dense_vector_free(after_lag, chol);
    linear_dense_sub_matrix_free(input_after_lag, chol);
}

void echo_train_readout( const unsigned int lag
                       , echo_dynamics* dynamics , echo_state* initial
		       , echo_readin* readin     , echo_series* input
                       , echo_series* target
	               , echo_readout* out_readout
	               , cholmod_common* chol ) {
    echo_state* initial_copy = cholmod_copy_dense(initial, chol);
    echo_series* target_copy =
    	cholmod_copy_dense(target, chol);
    echo_unsafe_train_readout( lag    , dynamics , initial_copy
	                     , readin , input    , target_copy
	                     , out_readout
	                     , chol );
    linear_dense_vector_free(initial_copy, chol);
    linear_dense_vector_free(target_copy, chol);
}

// TODO: change time series representation to column vector?
// -> input and target are expected to be column vectors.
// TODO: We should join readin and dynamics into one thing!
void echo_unsafe_train_readout( const unsigned int lag
                              , echo_dynamics* dynamics , echo_state* initial
		              , echo_readin* readin     , echo_series* input
                              , echo_series* target
	                      , echo_readout* out_readout
	                      , cholmod_common* chol ) {
    #ifndef NDEBUG
    check_valid_dynamics(    "echo_train_readout", dynamics                            );
    check_compatible_state(  "echo_train_readout", initial     , dynamics              );
    check_compatible_readin( "echo_train_readout", readin      , input    , dynamics   );
    check_compatible_readout("echo_train_readout", out_readout , dynamics , input->ncol);
    check_valid_train(       "echo_train_readout", lag         , input    , target     );
    #endif

    const int reservoir_size = dynamics->nrow;

    // TODO: I really want that "clone/from_prototype" procedure now...
    echo_state* after_lag = linear_dense_vector_alloc(initial->nzmax, chol);

    // Throw out the reservoir states we aren't interested in
    echo_unsafe_push_n(lag, dynamics, initial, readin, input, after_lag, chol);
    
    dense_sub_matrix* input_after_lag = 
	linear_dense_matrix_slice_rows(lag, input->nrow, input, chol);

    // WARNING: According to Valgrind parts of this matrix aren't initialised!
    dense_matrix* reservoir_states =
        linear_dense_matrix_alloc(reservoir_size + 1 + input->ncol, target->nrow, chol);

    // Collect the states we _are_ interested in
    echo_steps_n( target->nrow
                , dynamics, after_lag
		, readin, input_after_lag
		, reservoir_states
		, chol );

    //linear_dump_dense_matrix(reservoir_states, chol);

    linear_unsafe_dense_lse(reservoir_states, target, out_readout, chol);

    linear_dense_sub_matrix_free(input_after_lag, chol);
    linear_dense_vector_free(after_lag, chol);
    linear_dense_matrix_free(reservoir_states, chol);
}
