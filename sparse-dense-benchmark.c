#include <stdlib.h>
#include <stdio.h>

#include <time.h>

#include <gsl/gsl_rng.h>

#include <openblas/cblas.h>

#include <cholmod.h>

cholmod_dense* random_dense( const int nrow, const int ncol, const double fill
                           , const gsl_rng* gen, cholmod_common* chol) {
    cholmod_dense* result = cholmod_allocate_dense(nrow, ncol, nrow, CHOLMOD_REAL, chol);

    int range = result->nzmax;
    double* x = (double*)result->x;
    double tmp;
    double twiddle = 1/fill;
    for(int i = 0; i < range; ++i) {
	tmp  = gsl_rng_uniform(gen);
	x[i] = (tmp < fill)*twiddle*tmp;
    }

    return result;
}

int main(int argc, char** argv) {
    const int size = 500;
    const int replicates = 400;

    cholmod_common chol;
    cholmod_start(&chol);
    chol.print = 3;

    openblas_set_num_threads(1);
    
    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    
    cholmod_dense*  matrix_dense  = random_dense(size, size, 0.25, gen, &chol);
    cholmod_sparse* matrix_sparse = cholmod_dense_to_sparse(matrix_dense, 1, &chol);

    cholmod_dense* in_vec  = random_dense(size, 1, 1, gen, &chol);
    cholmod_dense* out_vec = cholmod_allocate_dense(size, 1, size, CHOLMOD_REAL, &chol);

    clock_t timer = 0;
    clock_t start_time;

    // Sparse
    cholmod_sdmult(matrix_sparse, 0, (double[2]){1, 1}, (double[2]){0, 0}, in_vec, out_vec, &chol);
    for(int i = 0; i < replicates; ++i) {
	start_time = clock();
	cholmod_sdmult( matrix_sparse, 0
		      , (double[2]){1, 1}, (double[2]){0, 0}
		      , in_vec, out_vec
		      , &chol);
	timer += clock() - start_time;
    }
    
    printf("Sparse: ~%f cycles\n", ((double)timer)/replicates);

    // Dense 
    timer = 0;
    double* matrix_dense_raw = matrix_dense->x;
    double* in_vec_raw  = in_vec->x;
    double* out_vec_raw = out_vec->x;
    cblas_dgemv( CblasRowMajor, CblasNoTrans
	       , size, size, 1
	       , matrix_dense_raw, size
	       , in_vec_raw, 1 
	       , 0, out_vec_raw, 1);
    for(int i = 0; i < replicates; ++i) {
	start_time = clock();
        cblas_dgemv( CblasRowMajor, CblasNoTrans
	           , size, size, 1
	           , matrix_dense_raw, size
	           , in_vec_raw, 1 
	           , 0, out_vec_raw, 1);
	timer += clock() - start_time;
    }
    
    printf("Dense: ~%f cycles\n", ((double)timer)/replicates);

    
    cholmod_free_dense(&out_vec, &chol);
    cholmod_free_dense(&in_vec, &chol);

    cholmod_free_dense(&matrix_dense, &chol);
    cholmod_free_sparse(&matrix_sparse, &chol);

    gsl_rng_free(gen);

    cholmod_finish(&chol);

    return EXIT_SUCCESS;
}
