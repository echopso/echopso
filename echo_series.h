#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "echo.h"

echo_series* sim_ar( const unsigned int length
		   , const unsigned int degree
		   , const double eps
		   , const double const * params
		   , const gsl_rng* gen
		     , cholmod_common* chol );
