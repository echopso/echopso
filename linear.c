/*********************************************************************************/
/* The MIT License (MIT)                                                         */
/*                                                                               */
/* Copyright (c) 2013 Tilo Wiklund, Ross Linscott                                */
/*                                                                               */
/* Permission is hereby granted, free of charge, to any person obtaining a copy  */
/* of this software and associated documentation files (the "Software"), to deal */
/* in the Software without restriction, including without limitation the rights  */
/* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     */
/* copies of the Software, and to permit persons to whom the Software is         */
/* furnished to do so, subject to the following conditions:                      */
/*                                                                               */
/* The above copyright notice and this permission notice shall be included in    */
/* all copies or substantial portions of the Software.                           */
/*                                                                               */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   */
/* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     */
/* THE SOFTWARE.                                                                 */
/*********************************************************************************/

#include <string.h>
#include <math.h>
#include "linear.h"

inline dense_matrix* linear_dense_identity( const unsigned int n
					  , const unsigned int m
                                          , cholmod_common* chol ) {
    return cholmod_eye(n, m, CHOLMOD_REAL, chol);
}

inline sparse_matrix* linear_sparse_identity( const unsigned int n
					    , const unsigned int m
                                            , cholmod_common* chol ) {
    return cholmod_speye(n, m, CHOLMOD_REAL, chol);
}

inline dense_matrix* linear_dense_matrix_alloc( const unsigned int n
					      , const unsigned int m
                                              , cholmod_common* chol ) {
    return cholmod_allocate_dense(n, m, n, CHOLMOD_REAL, chol);
}

inline void linear_dense_matrix_free( dense_matrix* matrix
				    , cholmod_common* chol ) {
    cholmod_free_dense(&matrix, chol);
}

inline dense_vector* linear_dense_vector_alloc( const unsigned int n
                                              , cholmod_common* chol ) {
    return linear_dense_matrix_alloc(n, 1, chol);
}

inline void linear_dense_vector_free( dense_vector* vector
				    , cholmod_common* chol ) {
    cholmod_free_dense(&vector, chol);
}


// TODO: Need to figure this one, check CHOLMOD documentation
/* sparse_matrix* linear_sparse_matrix_alloc( const unsigned int n, const unsigned int m */
/*                                       , const unsigned int size */
/*                                       , cholmod_common* chol) { */
/*     return cholmod_allocate_sparse(n, m, size, ) */
/* } */

inline void linear_sparse_matrix_free( sparse_matrix* matrix
				     , cholmod_common* chol ) {
    cholmod_free_sparse(&matrix, chol);
}

dense_vector* linear_dense_vector_zero( const unsigned int size
				     , cholmod_common* chol ) {
    return cholmod_zeros(size, 1, CHOLMOD_REAL, chol);
}

void linear_dense_vector_copy_to( dense_vector* to
				, dense_vector* from
				, char expect_memcpy
				, cholmod_common* chol ) {
    #ifndef NDEBUG
    if(expect_memcpy && (to->ncol != 1 || from->ncol != 1 || to->nrow < from->nrow)) {
	printf("dense_vector_copy_to: Expecting two column vectors, but got %zu x %zu and %zu x %zu\n", to->nrow, to->ncol, from->nrow, from->ncol);
	exit(EXIT_SUCCESS);
    }
    #endif

    if(expect_memcpy) 
	memcpy(to->x, from->x, sizeof(double)*(from->nrow));
    else {
	const unsigned int step_to = 
	    to->ncol == 1 ? 1 : to->d;
	const unsigned int step_from = 
	    from->ncol == 1 ? 1 : from->d;
	
	if(step_to == 1 && step_from == 1)
	    memcpy(to->x, from->x, sizeof(double)*(from->nrow));
	else {
	    const unsigned int range = 
		to->nzmax < from->nzmax ? to->nzmax : from->nzmax;
           
            double *to_p = to->x;
	    double *from_p = from->x;
	    for(int i = 0; i < range; ++i)
		to_p[step_to * i] = from_p[step_from * i];
	}
    }
}

// TODO: Unify with linear_dense_vector_copy_to
void linear_dense_matrix_copy_to( dense_vector* to
				, dense_vector* from
				, char expect_memcpy
				, cholmod_common* chol ) {
#ifndef NDEBUG
    if(from->nrow != to->nrow || from->ncol != to->ncol) {
	fprintf(stderr, "dense_matrix_copy_to: Non-matching dimensions");
	exit(EXIT_FAILURE);
    }
#endif
    if(expect_memcpy || (to->d == to->nrow && from->d == from->nrow)) {
	memcpy(to->x, from->x, sizeof(double)*to->nzmax);
    } else {
	fprintf(stderr, "dense_matrix_copy_to: Can't be bothered with this loop right now.");
	exit(EXIT_FAILURE);
    }
}


void linear_dense_vector_smooth(dense_vector *v, cholmod_common *chol){
    if (v -> nrow < 3){
	return;
    }

    int i;
    int m = v -> nrow - 2;
    double *x = v->x;
    double a = 1/(double)4;
    double *p = malloc(v -> nrow * sizeof(double));

    for (i = 2; i < m; i++){
	p[i] = a * (.5 * x[i-2] + x[i-1] + x[i] + x[i+1] + .5 * x[i+2]);
    }

    p[1] = (x[0] + x[1] + x[2]) / 3;
    p[v -> nrow - 2] = (x[v -> nrow - 3] + x[v -> nrow - 2] + x[v -> nrow - 1]) / 3;
    p[0] = .5 * (x[1] + x[0]);
    p[v -> nrow - 1] = .5 * (x[v -> nrow - 1] + x[v -> nrow - 2]);

    memcpy(x,p,v -> nrow * sizeof(double));
    free(p);
}


//TODO: 1 means copy, do we really want this?
inline sparse_matrix* linear_dense_to_sparse( dense_matrix* matrix
                                            , cholmod_common* chol ) {
    return cholmod_dense_to_sparse(matrix, 1, chol);
}

inline dense_matrix* linear_sparse_to_dense( sparse_matrix* matrix
                                           , cholmod_common* chol ) {
    return cholmod_sparse_to_dense(matrix, chol);    
}

sparse_matrix* linear_sparse_matrix_scale( double coef, sparse_matrix* matrix
                                         , cholmod_common* chol ) {
    sparse_matrix* result = cholmod_copy_sparse(matrix, chol);
    linear_unsafe_sparse_matrix_scale(coef, result, chol);
    return result;
}

//TODO: maybe rewrite this with a plain loop, this seems rather silly...
void linear_unsafe_sparse_matrix_scale( const double coef
				      , sparse_matrix* matrix
                                      , cholmod_common* chol ) {
    cholmod_dense* ptrcoef = linear_dense_matrix_alloc(1, 1, chol);
    ((double*) ptrcoef->x)[0] = coef;
    cholmod_scale(ptrcoef, CHOLMOD_SCALAR, matrix, chol);
    linear_dense_matrix_free(ptrcoef, chol);
}

dense_matrix* linear_dense_matrix_scale( const double coef
				       , dense_matrix* matrix
                                       , cholmod_common* chol ) {
    dense_matrix* result = cholmod_copy_dense(matrix, chol);
    linear_unsafe_dense_matrix_scale(coef, result, chol);
    return result;
}

void linear_unsafe_dense_matrix_scale( const double coef
				     , dense_matrix* matrix
                                     , cholmod_common* chol ) {
    int range = matrix->nzmax;
    double* x = (double*)matrix->x;
    for(int i = 0; i < range; ++i) 
        x[i] *= coef;
}

dense_vector* linear_dense_vector_scale( const double coef
				       , dense_vector* vector
                                       , cholmod_common* chol ) {
    return (dense_vector*) linear_dense_matrix_scale( coef
                                                    , (dense_matrix*) vector
                                                    , chol );
}

void linear_unsafe_dense_vector_scale( const double coef
				     , dense_vector* vector
                                     , cholmod_common* chol ) {
    linear_dense_matrix_scale(coef, vector, chol);
}

sparse_matrix* linear_sparse_matrix_add( sparse_matrix* matrix1
				       , sparse_matrix* matrix2
                                       , cholmod_common* chol ) {
    return cholmod_add( matrix1, matrix2
                      , (double[2]){1, 0}, (double[2]){1, 0}
                      , 1, 1, chol );
}

dense_matrix* linear_dense_matrix_add( dense_matrix* matrix1
				     , dense_matrix* matrix2
                                     , cholmod_common* chol ) {
    dense_matrix* result = cholmod_copy_dense(matrix1, chol);
    linear_unsafe_dense_matrix_add(result, matrix2, chol);
    return result;
}

dense_vector* linear_dense_vector_add( dense_vector* vector1
				     , dense_vector* vector2
                                     , cholmod_common* chol ) {
    return (dense_vector*) linear_dense_matrix_add( (dense_matrix*) vector1
                                                  , (dense_matrix*) vector2
                                                  , chol );
}

void linear_unsafe_dense_matrix_add( dense_matrix* matrix1, dense_matrix* matrix2
                                   , cholmod_common* chol) {
    int range = matrix1->nzmax;
    double* x = (double*)matrix1->x;
    double* y = (double*)matrix2->x;
    for(int i = 0; i < range; ++i)
        x[i] += y[i];
}

void linear_unsafe_dense_vector_add( dense_vector* vector1, dense_vector* vector2
                                   , cholmod_common* chol) {
    linear_unsafe_dense_matrix_add( (dense_matrix*)vector1
                                  , (dense_matrix*)vector2
                                  , chol);
}

inline void linear_sparse_apply( sparse_matrix* matrix, dense_vector* vector
                               , dense_vector* out_vector
                               , cholmod_common* chol) {
    cholmod_sdmult( matrix, 0
                  , (double[2]){1, 0}, (double[2]){0, 0}
                  , vector, out_vector
                  , chol );
}

// TODO: testme!
inline void linear_dense_apply( dense_matrix* matrix, dense_vector* vector
                              , dense_vector* out_vector
                              , cholmod_common* chol) {
    cblas_dgemv( CblasColMajor, CblasNoTrans
               , matrix->nrow, matrix->ncol, 1
               , matrix->x, matrix->nrow
		 // NOTE: x increment to accomodate row slices
	       , vector->x, vector->ncol == 1 ? 1 : vector->d 
               , 0, out_vector->x, out_vector->ncol == 1 ? 1 : out_vector->d );
}

dense_matrix* linear_dense_transpose(dense_matrix *matrix, cholmod_common *chol){
  dense_matrix* t = linear_dense_matrix_alloc(matrix->ncol, matrix->nrow, chol);
  int i,j;
  for (j = 0; j < matrix->ncol; j++){
    for (i = 0; i < matrix->nrow; i++){
      ((double*)t->x)[j + i * t->nrow] = ((double*)matrix->x)[i + j * matrix->nrow];
    }
  }

  return t;
}

void linear_unsafe_dense_transpose(dense_matrix *matrix, cholmod_common *chol){
  sparse_matrix* m = cholmod_dense_to_sparse(matrix, 1, chol);
  sparse_matrix* t = cholmod_transpose(m, 1, chol);
  dense_matrix* x = cholmod_sparse_to_dense(t, chol);
  void *p = matrix->x;
  memcpy(matrix, x, sizeof(dense_matrix));
  matrix->x = p;
  memcpy(matrix->x, x->x, x->nzmax * sizeof(double));
  linear_sparse_matrix_free(m, chol);
  linear_sparse_matrix_free(t, chol);
  linear_dense_matrix_free(x, chol);
}

// WARNING: Takes the _transposed_ covarate matrix!
void linear_dense_lse( dense_matrix* covariateT, dense_vector* response
                     , dense_matrix* out_hat
                     , cholmod_common* chol ) {
    dense_matrix* covariate_copy = cholmod_copy_dense(covariateT, chol);
    dense_vector* response_copy  = cholmod_copy_dense(response, chol);
    
    linear_unsafe_dense_lse(covariate_copy, response_copy, out_hat, chol);

    linear_dense_matrix_free(covariate_copy, chol);
    linear_dense_vector_free( response_copy, chol);
}

// WARNING: Takes the _transposed_ covarate matrix!
void linear_unsafe_dense_lse( dense_matrix* covariateT, dense_vector* response
                            , dense_matrix* out_hat
                            , cholmod_common* chol ) {
    const unsigned int nrow = covariateT->nrow;
    const unsigned int ncol = covariateT->ncol;

    clapack_dgels( CblasColMajor          , CblasTrans
                 , nrow                   , ncol          , 1
		 , (double*)covariateT->x , covariateT->d
		 , (double*)response->x   , response->d   );

    memcpy(out_hat->x, response->x, sizeof(double)*nrow);
}

// Does NOT take the transposed matrix.
void linear_unsafe_dense_lse2( dense_matrix* covariate, dense_vector* response
                            , dense_matrix* out_hat
                            , cholmod_common* chol ) {
    const unsigned int nrow = covariate->nrow;
    const unsigned int ncol = covariate->ncol;

    if (ncol > nrow || nrow != response->nrow){
      fprintf(stderr, "error: LSE requires nrow >= ncol and nrow == response->nrow\n");
      exit(EXIT_FAILURE);
    }

    clapack_dgels( CblasColMajor          , CblasNoTrans
                 , nrow                   , ncol          , 1
		 , (double*)covariate->x , covariate->d
		 , (double*)response->x   , response->d   );

    memcpy(out_hat->x, response->x, sizeof(double)*ncol);
}

double linear_dense_distance(dense_vector *a, dense_vector *b){
  int i;
  double s = 0;
  double t;

  int a_inc = a->ncol == 1 ? 1 : a->d;
  int b_inc = b->ncol == 1 ? 1 : b->d;
  int a_num = fmax(a->nrow, a->ncol);
  int b_num = fmax(b->nrow, b->ncol);
  double *ap = a->x;
  double *bp = b->x;
  
  if (a_num != b_num || (a->ncol > 1 && a->nrow > 1) || (b->ncol > 1 && b->nrow > 1)){
    fprintf(stderr,"linear_dense_distance: invalid dimension! (%zu x %zu, %zu x %zu)\n", a->nrow, a->ncol, b->nrow, b->ncol);
    exit(EXIT_FAILURE);
  }

  for (i = 0; i < a_num; i++){
    t = ap[i * a_inc] - bp[i * b_inc];
    s += t * t;
  }

  return sqrt(s);
}

double linear_dense_distance2(dense_vector *a, dense_vector *b){
  int i;
  double s = 0;
  double t;

  int a_inc = a->ncol == 1 ? 1 : a->d;
  int b_inc = b->ncol == 1 ? 1 : b->d;
  int a_num = fmax(a->nrow, a->ncol);
  int b_num = fmax(b->nrow, b->ncol);
  double *ap = a->x;
  double *bp = b->x;
  
  if (a_num != b_num || (a->ncol > 1 && a->nrow > 1) || (b->ncol > 1 && b->nrow > 1)){
    fprintf(stderr,"linear_dense_distance2: invalid dimension! (%zu x %zu, %zu x %zu)\n", a->nrow, a->ncol, b->nrow, b->ncol);
    exit(EXIT_FAILURE);
  }

  for (i = 0; i < a_num; i++){
    t = ap[i * a_inc] - bp[i * b_inc];
    s += t * t;
  }

  return s;
}

dense_matrix* linear_dense_matrix_random( const unsigned int n, const unsigned int m
                                        , const gsl_rng* gen, distribution dist, const void* args
                                        , cholmod_common* chol ) {
    dense_matrix* result = linear_dense_matrix_alloc(n, m, chol);

    int range = result->nzmax;
    double* x = (double*)result->x;
    for(int i = 0; i < range; ++i) {
        x[i] = (*dist)(args, gen);
    }

    return result;
}

//TODO: This could be done without copying, I think, but then we need
//to include that extra argument in the to/from-sparse routines.
sparse_matrix* linear_sparse_matrix_random( const unsigned int n, const unsigned int m
                                          , const gsl_rng* gen, distribution dist, const void* args
                                          , cholmod_common* chol ) {
    dense_matrix* dense   = linear_dense_matrix_random(n, m, gen, dist, args, chol);
    sparse_matrix* sparse = linear_dense_to_sparse(dense, chol);
    linear_dense_matrix_free(dense, chol);
    return sparse;
}

dense_matrix* linear_dense_vector_random( const unsigned int n
                                        , const gsl_rng* gen, distribution dist, const void* args
                                        , cholmod_common* chol ) {
    return linear_dense_matrix_random(n, 1, gen, dist, args, chol);
}

void linear_dump_dense_matrix(dense_matrix* matrix, cholmod_common* chol) {
    int chol_print_push = chol->print;
    chol->print = 5;
    cholmod_print_dense(matrix, "Dense Matrix", chol);
    chol->print = chol_print_push;
}

void linear_dump_dense_vector(dense_vector* vector, cholmod_common* chol) {
    int chol_print_push = chol->print;
    chol->print = 5;
    cholmod_print_dense(vector, "Dense Vector", chol);
    chol->print = chol_print_push;
}

void linear_dump_sparse_matrix(sparse_matrix* matrix, cholmod_common* chol) {
    int chol_print_push = chol->print;
    chol->print = 5;
    cholmod_print_sparse(matrix, "Sparse Matrix", chol);
    chol->print = chol_print_push;
}

dense_matrix_column* linear_dense_matrix_columns( dense_matrix* matrix
						, cholmod_common* chol) {
    dense_matrix_column* result = malloc(sizeof(dense_matrix_column));
    //TODO: Rewrite this with a memcpy!!!
    result->nrow  = matrix->nrow;
    result->ncol  = 1;
    result->nzmax = matrix->nrow;
    result->d     = matrix->d;
    result->x     = matrix->x;
    result->z     = matrix->z;
    result->xtype = matrix->xtype;
    result->dtype = matrix->dtype;

    return result;
}

void linear_dense_matrix_columns_free( dense_matrix_column* column
				     , cholmod_common* chol) {
    free(column);
}

void linear_next_column( dense_matrix_column* column
		       , cholmod_common* chol ) {
    column->x = (void*)(((double*)column->x) + column->d);
}

dense_matrix_row* linear_dense_matrix_rows( dense_matrix* matrix
					  , cholmod_common* chol ) {
    dense_matrix_row* result = malloc(sizeof(dense_matrix_row));

    memcpy(result, matrix, sizeof(dense_matrix_row));
    result->nrow  = 1;
    result->nzmax = result->ncol;

    //NOTE: for some reason, cholmod requires result->nzmax == matrix->nzmax, rather than matrix->ncol

    /* result->ncol  = matrix->ncol; */
    /* result->nzmax = matrix->nzmax; */
    /* result->d     = matrix->d; */
    /* result->x     = matrix->x; */
    /* result->z     = matrix->z; */
    /* result->xtype = matrix->xtype; */
    /* result->dtype = matrix->dtype; */

    return result;
}

void linear_dense_matrix_rows_free( dense_matrix_row* row
				     , cholmod_common* chol) {
    free(row);
}

void linear_next_row( dense_matrix_row* row
		       , cholmod_common* chol ) {
    row->x = (void*)(((double*)row->x) + 1);
}

void linear_forward_columns_n( const int n
			     , dense_matrix_column* column
			     , cholmod_common* chol ) {
    column->x = (void*)(((double*)column->x) + n*(column->nzmax));
}

dense_sub_matrix* linear_dense_matrix_slice_columns( const unsigned int from
						   , const unsigned int to
						   , dense_matrix* matrix
						   , cholmod_common* chol ) {
    // TODO: SOME ERROR CHECKING PLZ!
    dense_sub_matrix* result = malloc(sizeof(dense_sub_matrix));
    //TODO: Rewrite this with a memcpy!!!
    result->nrow  = matrix->nrow;
    result->ncol  = to - from;
    result->nzmax = (result->nrow)*(to-from);
    result->d     = matrix->d;
    result->x     = (void*)(((double*)matrix->x) + from*(matrix->d));
    result->z     = matrix->z;
    result->xtype = matrix->xtype;
    result->dtype = matrix->dtype;

    return result;
}

dense_sub_matrix* linear_dense_matrix_slice_rows( const unsigned int from
						   , const unsigned int to
						   , dense_matrix* matrix
						   , cholmod_common* chol ) {
    // TODO: SOME ERROR CHECKING PLZ!
    dense_sub_matrix* result = malloc(sizeof(dense_sub_matrix));
    //TODO: Rewrite this with a memcpy!!!
    result->nrow  = to - from;
    result->ncol  = matrix->ncol;
    result->nzmax = (result->ncol)*(to-from);
    result->d     = matrix->d;
    result->x     = (void*)(((double*)matrix->x) + from);
    result->z     = matrix->z;
    result->xtype = matrix->xtype;
    result->dtype = matrix->dtype;

    return result;
}

void linear_dense_sub_matrix_free( dense_sub_matrix* submatrix
				 , cholmod_common* chol ) {
    free(submatrix);
}
