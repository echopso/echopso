#include <time.h>
#include <string.h>
#include <gsl/gsl_rng.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "echo_util.h"
#include "echo_series.h"

/* The network is expected to predict lookahead steps into the future. 
   Runs the network on the input, then uses the prediction result as input 
   to produce a LOOKAHEAD * STEPS steps prediction. 
   The network must be pre-trained.
*/

int main(int argc, char **argv){

    if (argc != 5){
	fprintf(stdout,"usage: %s network input lookahead steps\n", argv[0]);
	return 0;
    }

    int i;
    cholmod_common *chol = malloc(sizeof(cholmod_common));
    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);

    gsl_rng_set (gen, time(NULL));
    cholmod_start(chol);

    FILE *f_net = fopen(argv[1], "r");
    if (!f_net){
	fprintf(stderr,"failed to open network file: %s.\n", argv[1]);
	exit(EXIT_FAILURE);
    }
	
    echo_network net = echo_util_deserialise_network(f_net, chol);
    fclose(f_net);

    FILE *f_input = fopen(argv[2], "r");
    if (!f_net){
	fprintf(stderr,"failed to open input file: %s.\n", argv[2]);
	exit(EXIT_FAILURE);
    }
    echo_series *input = echo_util_read_doubles(f_input, chol);
    fclose(f_input);

    int lookahead = atoi(argv[3]);
    int steps = atoi(argv[4]);

    echo_series *prediction = echo_util_piecewise_prediction(net, steps, lookahead, input, gen, chol);

    for (i = 0; i < prediction -> nrow; i++){
	fprintf(stdout, "%f\n", ((double*)prediction -> x)[i]);
    }
    
    echo_util_net_free(net, chol);
    echo_util_series_free(prediction, chol);
    echo_util_series_free(input, chol);
    cholmod_finish(chol);
    gsl_rng_free(gen);
    free(chol);
    return 0;
}
