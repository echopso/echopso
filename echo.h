/*********************************************************************************/
/* The MIT License (MIT)                                                         */
/*                                                                               */
/* Copyright (c) 2013 Tilo Wiklund, Ross Linscott                                */
/*                                                                               */
/* Permission is hereby granted, free of charge, to any person obtaining a copy  */
/* of this software and associated documentation files (the "Software"), to deal */
/* in the Software without restriction, including without limitation the rights  */
/* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     */
/* copies of the Software, and to permit persons to whom the Software is         */
/* furnished to do so, subject to the following conditions:                      */
/*                                                                               */
/* The above copyright notice and this permission notice shall be included in    */
/* all copies or substantial portions of the Software.                           */
/*                                                                               */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   */
/* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     */
/* THE SOFTWARE.                                                                 */
/*********************************************************************************/

#ifndef ECHO_ECHO_H
#define ECHO_ECHO_H

#include "linear.h"

typedef sparse_matrix echo_dynamics;
typedef dense_matrix  echo_readout;
typedef dense_matrix  echo_readin;

typedef dense_matrix echo_series;

typedef dense_vector echo_state;

// TODO: Add a input->output connections?

// Note: The reason for not including the "readin" layer in the
// dynamics is due to memory management (when evaluating a network the "input" 
// time series will already be stored somewhere, while the state of the 
// reservoir is allocated by the evaluation procedure. Not doing it this way 
// would either force the user to include extra space in the time series data
// (which is insane, as doing so couples the represenation to a particular 
// reservoir size used for training it) or do a bunch of fiddly and slow copying
// of read-only data).
typedef struct echo_network_s {
    echo_readin*   readin; 
    echo_dynamics* dynamics;
    echo_readout*  readout;
} echo_network;

typedef struct echo_sample_s {
    echo_series** inputs;
    echo_series** targets;
    const unsigned int number;
} echo_sample;

// NOTE: Only statically known activation functions for now
double identity(double x);

#ifndef ECHO_ACTIVATION
#define ECHO_ACTIVATION tanh
#endif 

// TODO: Add some constructor/allocation procedures

/* PRIVATE INTERFACE */

void check_valid_dynamics( const char* name
			   , const echo_dynamics const * dynamics);

void check_compatible_state( const char* name
			   , const echo_state const * state
			   , const echo_dynamics const * dynamics);

void check_compatible_readin( const char* name
			    , const echo_readin const * readin
			    , const echo_series const * input
			    , const echo_dynamics const * dynamics );

void check_compatible_readout( const char* name
			     , const echo_readout const * readout
			     , const echo_dynamics const * dynamics
			     , const int input_size);

void check_enough_output( const char* name
			, const echo_series const * output
			, const unsigned int length);

void check_valid_output_width( const char* name
			     , const echo_series const * output
			     , const unsigned int width );

void check_valid_train( const char* name
                      , const unsigned int lag
	              , echo_series* input
		      , echo_series* target );

void echo_steps_n( const unsigned int num 
                 , echo_dynamics* dynamics, echo_state* current
		 , echo_readin* readin, echo_series* input
                 , echo_series* out_output
		 , cholmod_common* chol );

// TODO: Think of a better name
void echo_push_n( const unsigned int num
                , echo_dynamics* dynamics , echo_state* initial
		, echo_readin* readin     , echo_series* input
                , echo_state* out_output
	        , cholmod_common* chol );

void echo_unsafe_push_n( const unsigned int num
                       , echo_dynamics* dynamics , echo_state* initial
		       , echo_readin* readin     , echo_series* input
                       , echo_state* out_output
	               , cholmod_common* chol );

/* PUBLIC INTERFACE */

// NOTE: I don't think we want this...
/* void echo_run( echo_network net */
/*              , echo_series* input, echo_series* output */
/*              , cholmod_common* chol ); */

// TODO: Should we move the lag into the network?
void echo_run_n( const unsigned int num , const unsigned int lag
	       , echo_network net       , echo_state* initial
               , echo_series* input     
	       , echo_series* out_output
               , cholmod_common* chol );

void echo_unsafe_run_n( const unsigned int num , const unsigned int lag
		      , echo_network net       , echo_state* initial
                      , echo_series* input
		      , echo_series* out_output
                      , cholmod_common* chol );

void echo_train_readout( const unsigned int lag
                       , echo_dynamics* net  , echo_state* initial
		       , echo_readin* readin , echo_series* input
                       , echo_series* target
	               , echo_readout* out_readout
		       , cholmod_common* chol );

void echo_unsafe_train_readout( const unsigned int lag
                              , echo_dynamics* net  , echo_state* initial
		              , echo_readin* readin , echo_series* input
                              , echo_series* target
	                      , echo_readout* out_readout
		              , cholmod_common* chol );

void dump_matlab(dense_matrix* m);

#endif
