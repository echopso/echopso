function y = generate_mackey_glass(tau, tspan, x0, predict)

sol = dde23(@fprime, tau, x0, tspan);

y = deval(sol, tspan);
input = y(1:(end-predict));
target = y((1+predict):end);

fname = ['MG-series_tau=',num2str(tau),'_n=', num2str(numel(tspan)),'_x0=',num2str(x0),'_steps=',num2str(predict)];

dlmwrite(['input-',fname], input');
dlmwrite(['target-',fname], target');

end

function dxdt = fprime(~, x, z)
dxdt = 0.2 * z / (1 + z^10) - 0.1 * x;
end