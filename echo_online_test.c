#include "linear.h"
#include "echo.h"
#include "echo_util.h"
#include "echo_series.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define M_PI 3.141596

int main(int argc, char** argv) {
    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));
    // TODO: There's some issue when the reservoir is bigger than 
    // the number of smaples (some paramters wrong in some multiplication
    // somewhere). Figure this out...

    unsigned int reservoir_size = 200;
    unsigned int input_size = 2000;
    unsigned int shift = 1;
    double eta = 0.02;
    double decay = 0.99;

    if (argc != 6){
      fprintf(stdout,"usage: %s reservoir_size input_size prediction_shift eta decay.\n", argv[0]);
      return 0;
    }

    reservoir_size = atoi(argv[1]);
    input_size = atoi(argv[2]);
    shift = atoi(argv[3]);
    eta = atof(argv[4]);
    decay = atof(argv[5]);

    int i;

    echo_series* ar_series = sim_ar(input_size, 2, 0.2, (double[2]){0.72, -0.2}, gen, &chol);

    echo_network net = echo_util_make_network(reservoir_size, 1, 1, gen, &chol);

    echo_series* input = echo_util_input_series_for(net, input_size, &chol);
    memcpy(input->x, ar_series->x, sizeof(double)*input_size);

    echo_series* target = echo_util_output_series_for(net, input_size - shift, &chol);
    memcpy(target->x, (void*)(((double*)ar_series->x)+shift), sizeof(double)*(input_size - shift));

    echo_series* error;
    echo_series* run_result = echo_util_output_series_for(net, input_size, &chol);
    echo_state* initial  = echo_util_make_state_for(net, gen, &chol);

    error = echo_util_online_train(net, input, target, run_result, eta, decay, gen, &chol);

    for(i = 0; i < input_size; ++i) {
      printf("%d\t%f\t%f\t%f\n"  , i 
	     , ((double*)run_result->x)[i]
	     , ((double*)target->x)[i]
	     , ((double*)error->x)[i]);
    }

    echo_util_net_free(net, &chol);

    linear_dense_vector_free(  initial   , &chol);
    linear_dense_matrix_free(  input     , &chol);
    linear_dense_matrix_free(  target    , &chol);
    linear_dense_matrix_free(  run_result, &chol);
    linear_dense_matrix_free(  error     , &chol);
    linear_dense_vector_free(  ar_series , &chol);
    
    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
