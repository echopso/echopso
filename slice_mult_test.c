
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "linear.h"
//#include "echo.h"

const double support[10] = { -1.21, -0.34, 0, 0, 0, 0, 0, 0, 0.51, 1.10 };
double sparse_dist(void* null, const gsl_rng* gen) {
    return support[gsl_rng_uniform_int(gen, 10)];
}

int main(int argc, char** argv) {
    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);

    const unsigned int n = 100;

    int sel_row = 25;

    dense_matrix* A = 
        linear_dense_matrix_random( n, n, gen, &sparse_dist, (void*)0, &chol );

    dense_matrix* B = 
        linear_dense_matrix_random( n, n 
				  , gen, &sparse_dist, (void*)0
				  , &chol );

    dense_vector* x = linear_dense_matrix_rows(B, &chol); 
    dense_vector* y = linear_dense_vector_alloc(n, &chol);
    dense_vector* y2 = linear_dense_vector_alloc(n, &chol);
    dense_vector* x2 = linear_dense_vector_alloc(n, &chol);

    int i;
    for (i = 0; i < n; i++){
      ((double*)x2->x)[i] = ((double*)B->x)[sel_row + i * B->nrow];
    }

    for (i = 0; i < sel_row; i++){
      linear_next_row(x, &chol);
    }


    //linear_next_row(x, &chol);

    linear_dense_apply(A, x, y, &chol);
    linear_dense_apply(A, x2, y2, &chol);

    /* linear_dump_dense_matrix(A, &chol); */
    /* linear_dump_dense_matrix(B, &chol); */
    /* linear_dump_dense_vector(x, &chol); */
    /* linear_dump_dense_vector(y, &chol); */
    /* linear_dump_dense_vector(x2, &chol); */
    /* linear_dump_dense_vector(y2, &chol); */

    dense_vector* y3 = linear_dense_vector_scale(-1, y2, &chol);
    linear_unsafe_dense_vector_add(y, y3, &chol);

    fprintf(stdout,"error: %.3e.\n", cholmod_norm_dense(y, 2, &chol));
    
    linear_dense_matrix_rows_free(x, &chol);
    linear_dense_matrix_free(A, &chol);
    linear_dense_matrix_free(B, &chol);
    linear_dense_vector_free(y, &chol);
    linear_dense_vector_free(y2, &chol);
    linear_dense_vector_free(x2, &chol);

    cholmod_finish(&chol);
    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
