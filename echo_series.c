#include "echo_series.h"

echo_series* sim_ar( const unsigned int length
		   , const unsigned int degree
		   , const double eps
		   , const double const * params
		   , const gsl_rng* gen
		   , cholmod_common* chol ) {

    echo_series* result_series = 
	linear_dense_vector_alloc(length, chol);

    double* result = (double*) result_series->x;

    double acc;
    for(int i = 0; i < degree; ++i) {
        acc = eps * gsl_ran_gaussian(gen, 1);
        for(int k = 0; k < i; ++k) {
            acc += params[k] * result[i - k - 1];
        }
        result[i] = acc;
    }
    for(int i = degree; i < length; ++i) {
        acc = eps * gsl_ran_gaussian(gen, 1);
        for(int k = 0; k < degree; ++k) {
            acc += params[k] * result[i - k - 1];
        }
        result[i] = acc;
    }

    return result_series;
}

